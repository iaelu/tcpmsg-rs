use std::error::Error;

use encoding_rs::mem::decode_latin1;
use tcpmsg_rs::server::Server;

const MYSERVER_PORT: u16 = 8723;
const MYTYPE_SIREN: i32 = 10;
const MYTYPE_ADDR: i32 = 11;
const MYTYPE_DIR: i32 = 12;
const MYTYPE_EXIT: i32 = 13;

fn main() -> Result<(), Box<dyn Error>> {
    let mut server = Server::new(MYSERVER_PORT, 1024)?;

    server.add_msgtype(MYTYPE_SIREN, "siren".as_bytes(), 4)?;
    server.add_msgtype(MYTYPE_ADDR, "adr".as_bytes(), 4)?;
    server.add_msgtype(MYTYPE_DIR, "dir".as_bytes(), 4)?;
    server.add_msgtype(MYTYPE_EXIT, "exit".as_bytes(), 4)?;

    let mut exit_now = false;
    while !exit_now {
        match server.get_message() {
            Ok(None) => {}
            Ok(Some(msg)) => {
                // let data = msg.data();
                // if let Some(msgtype) = server.get_msgtype(msg.msgtype()) {
                //     println!(
                //         "[type:{}({})][size:{}][{}]",
                //         decode_latin1(msgtype.sign()),
                //         msgtype.value(),
                //         data.len(),
                //         decode_latin1(data)
                //     );
                // }
                let message = match msg.msgtype() {
                    MYTYPE_SIREN => {
                        // println!("mon beau siren : {}", decode_latin1(data));
                        Some("--siren--".as_bytes())
                    }
                    MYTYPE_ADDR => {
                        // println!("ma belle adresse : {}", decode_latin1(data));
                        Some("--adr--".as_bytes())
                    }
                    MYTYPE_DIR => {
                        // println!("mon bio dirigeant : {}", decode_latin1(data));
                        Some("--dir--".as_bytes())
                    }
                    MYTYPE_EXIT => {
                        // println!("client is calling for exit(): {}", decode_latin1(data));
                        exit_now = true;
                        Some("--dir--".as_bytes())
                    }
                    _ => {
                        eprintln!("ERROR ABORT ALL YOUR BASE");
                        None
                    }
                };
                if let Some(message) = message {
                    if let Err(err) = server.queue_message_by_value(msg.msgtype(), message, &msg) {
                        eprintln!("server.queue_message_by_value error: {}", err);
                    }
                }
                if let Err(err) = server.send_messages_for(&msg) {
                    eprintln!("server.send_messages error: {}", err);
                }
            }
            Err(err) => return Err(err),
        };
        match server.send_messages() {
            Ok(0) => {}
            Ok(_) => print!("."),
            Err(err) => eprintln!("server.send_messages error: {}", err),
        }
    }

    Ok(())
}
