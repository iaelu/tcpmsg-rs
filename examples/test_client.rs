use encoding_rs::mem::decode_latin1;
use std::{env::args, error::Error, net::SocketAddr, thread::sleep, time::Duration};
use tcpmsg_rs::client::Client;

const MYTYPE_SIREN: i32 = 10;
const MYTYPE_ADDR: i32 = 11;
const MYTYPE_DIR: i32 = 12;
const MYTYPE_EXIT: i32 = 13;

const TEST_MSG_SZ: usize = 4999;
const TEST_MSG_NUM: usize = 1000;

fn main() -> Result<(), Box<dyn Error>> {
    let hostaddr = args().nth(1).expect("usage: program [hostaddr]");

    let addr: SocketAddr = hostaddr.parse()?;
    let mut client = Client::new(addr)?;

    client.add_msgtype(MYTYPE_SIREN, "siren".as_bytes(), 4)?;
    client.add_msgtype(MYTYPE_ADDR, "adr".as_bytes(), 4)?;
    client.add_msgtype(MYTYPE_DIR, "dir".as_bytes(), 4)?;
    client.add_msgtype(MYTYPE_EXIT, "exit".as_bytes(), 4)?;

    let mut sent: usize = 0;
    let mut recv: usize = 0;
    let mymessage = "Z".repeat(TEST_MSG_SZ);

    for _i in 0..TEST_MSG_NUM {
        if let Err(err) = client.queue_message_by_value(MYTYPE_SIREN, "123456789".as_bytes()) {
            eprintln!("client.queue_message_by_value MYTYPE_SIREN");
            return Err(err);
        }
        sent += 1;

        if let Err(err) = client.queue_message_by_value(MYTYPE_ADDR, "5 rue pouet".as_bytes()) {
            eprintln!("client.queue_message_by_value MYTYPE_SIREN");
            return Err(err);
        }
        sent += 1;

        if let Err(err) = client.queue_message_by_value(MYTYPE_DIR, mymessage.as_bytes()) {
            eprintln!("client.queue_message_by_value MYTYPE_SIREN");
            return Err(err);
        }
        sent += 1;

        if let Err(err) = client.send_message() {
            eprintln!("client.send_msg error #1");
            return Err(err.into());
        }

        loop {
            match client.get_message() {
                Ok(Some(msg)) => match msg.msgtype() {
                    MYTYPE_SIREN => {
                        println!("mon beau siren : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_ADDR => {
                        println!("ma belle adresse : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_DIR => {
                        println!("mon bio dirigeant : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_EXIT => {
                        println!("server exit : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    _ => println!("ERROR ABORT ALL YOUR BASE !"),
                },
                Ok(None) => break,
                Err(err) => return Err(err),
            }
        }
        print!("=");
    }

    if let Err(err) = client.queue_message_by_value(MYTYPE_EXIT, "exit now".as_bytes()) {
        eprintln!("client.queue_message_by_value MYTYPE_EXIT");
        return Err(err);
    }
    sent += 1;

    loop {
        let to_send = client.messages_to_send();
        if to_send == 0 {
            break;
        }
        let msgsent = match client.send_message() {
            Ok(n) => n,
            Err(err) => {
                eprintln!("client.send_msg error #2");
                return Err(err.into());
            }
        };
        print!("[{}]{{{}}}", msgsent, to_send);
        loop {
            match client.get_message() {
                Ok(Some(msg)) => match msg.msgtype() {
                    MYTYPE_SIREN => {
                        println!("mon beau siren : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_ADDR => {
                        println!("ma belle adresse : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_DIR => {
                        println!("mon bio dirigeant : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_EXIT => {
                        println!("server exit : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    _ => println!("ERROR ABORT ALL YOUR BASE !"),
                },
                Ok(None) => break,
                Err(err) => return Err(err),
            }
        }
        if msgsent == 0 {
            sleep(Duration::from_secs(1));
        }
    }

    println!("LAST TURN sent: {}, recv: {}", sent, recv);

    while recv < sent {
        loop {
            match client.get_message() {
                Ok(Some(msg)) => match msg.msgtype() {
                    MYTYPE_SIREN => {
                        println!("mon beau siren : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_ADDR => {
                        println!("ma belle adresse : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_DIR => {
                        println!("mon bio dirigeant : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    MYTYPE_EXIT => {
                        println!("server exit : {}", decode_latin1(msg.data()));
                        recv += 1;
                    }
                    _ => println!("ERROR ABORT ALL YOUR BASE !"),
                },
                Ok(None) => break,
                Err(err) => return Err(err),
            }
        }
    }

    println!("DONE sent: {}, recv: {}", sent, recv);

    Ok(())
}
