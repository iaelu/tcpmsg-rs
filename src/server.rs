use std::{
    error::Error,
    io::{self, ErrorKind},
    net::SocketAddr,
    ops::Sub,
    time::{Duration, Instant},
};

use mio::{net::TcpListener, Events, Interest, Poll};

use crate::{
    config::ServerConfig,
    config::{SrvConfig, StreamConfig},
    message::Message,
    msgtype::*,
    stream::Stream,
};

pub struct Server<'a> {
    inner: TcpListener,
    poll: Poll,
    events: Events,
    stream: Vec<Stream>,
    msgtypes: Vec<MsgType<'a>>,

    last_event: Instant,
    last_accept: Instant,
    refuse: bool,
    noaccept: bool,
    maxstream: usize,
    streams: usize,
    config: SrvConfig,
}

impl<'a> Server<'a> {
    pub fn new(port: u16, maxstreams: usize) -> Result<Server<'a>, Box<dyn Error>> {
        Server::new_with_config(port, maxstreams, SrvConfig::default())
    }

    pub fn new_with_config(
        port: u16,
        maxstreams: usize,
        config: SrvConfig,
    ) -> Result<Server<'a>, Box<dyn Error>> {
        let saddr: SocketAddr = format!("0.0.0.0:{}", port).parse()?;
        let ln = TcpListener::bind(saddr)?;

        let poll = Poll::new()?;
        let events = Events::with_capacity(maxstreams);

        let mut stream: Vec<Stream> = Vec::with_capacity(maxstreams);
        stream.resize_with(maxstreams, Stream::default);
        let mut server = Server {
            inner: ln,
            poll,
            events,
            stream,
            msgtypes: Vec::new(),

            last_event: Instant::now(),
            last_accept: Instant::now(),
            refuse: false,
            noaccept: false,
            maxstream: 0,
            streams: 0,

            config,
        };
        server.config.set_server_port(port);
        server.config.set_max_streams(maxstreams);

        Ok(server)
    }

    pub fn add_msgtype(&mut self, value: i32, sign: &'a [u8], size: usize) -> Result<(), String> {
        if size > DEFAULT_MAX_SIZE_SIZE {
            return Err(format!(
                "size ({}) > MAX_SIZE_SIZE ({})",
                size, DEFAULT_MAX_SIZE_SIZE
            ));
        }
        if sign.len() > DEFAULT_MAX_SIGN_SIZE {
            return Err(format!(
                "size ({}) > MAX_SIGN_SIZE ({})",
                size, DEFAULT_MAX_SIGN_SIZE
            ));
        }
        self.msgtypes.push(MsgType::new(value, sign, size));
        Ok(())
    }

    pub fn add_msgtypes(&mut self, request_types: &'a [RequestType]) -> Result<(), String> {
        for rt in request_types {
            self.add_msgtype(rt.value, rt.sign, rt.size)?;
        }
        Ok(())
    }

    pub fn get_message(&mut self) -> Result<Option<Message>, Box<dyn Error>> {
        let nextc = self.stream[0..self.maxstream]
            .iter()
            .enumerate()
            .max_by(|(_, a), (_, b)| a.rcv().cmp(&b.rcv()))
            .map_or(0, |(c, _)| c);

        for c in nextc..self.maxstream {
            let stream = &mut self.stream[c];
            match stream.get_message(&self.msgtypes, &self.config)? {
                (Some(msg), false) => return Ok(Some(msg)),
                (None, true) => {
                    log::debug!(
                        "Closing stream #1: {}, nextc: {}, maxstream: {}",
                        c,
                        nextc,
                        self.maxstream
                    );
                    self.poll.registry().deregister(stream)?;
                    self.streams -= 1;
                    while self.maxstream > 0 && self.stream[self.maxstream - 1].is_state_avail() {
                        self.maxstream -= 1;
                    }
                }
                _ => {}
            }
        }
        for c in 0..nextc {
            let stream = &mut self.stream[c];
            match stream.get_message(&self.msgtypes, &self.config)? {
                (Some(msg), false) => return Ok(Some(msg)),
                (None, true) => {
                    log::debug!(
                        "Closing stream #2: {}, nextc: {}, maxstream: {}",
                        c,
                        nextc,
                        self.maxstream
                    );
                    self.poll.registry().deregister(stream)?;
                    self.streams -= 1;
                    while self.maxstream > 0 && self.stream[self.maxstream - 1].is_state_avail() {
                        self.maxstream -= 1;
                    }
                }
                _ => {}
            }
        }

        let now = Instant::now();
        if now.sub(self.last_event) > self.config.event_frequence() {
            self.last_event = now;

            self.poll
                .poll(&mut self.events, Some(Duration::from_millis(1)))?;

            for event in self.events.iter() {
                if !event.is_readable() {
                    continue;
                }
                let stream = &mut self.stream[event.token().0];
                stream.receive(&self.config)?;
                // let's register for more read since we do not loop until wouldblock
                self.poll
                    .registry()
                    .reregister(stream, event.token(), Interest::READABLE)?;
            }
        }

        let now = Instant::now();
        if !self.noaccept && now.sub(self.last_accept) > self.config.accept_frequence() {
            self.last_accept = now;
            match self.inner.accept() {
                Ok((conn, _)) if self.refuse => {
                    log::debug!("server is in refuse mode: closing connection");
                    drop(conn);
                }
                Ok((conn, addr)) => {
                    let mut st: usize = 0;
                    while st < self.config.max_streams() {
                        if self.stream[st].is_state_avail() {
                            break;
                        }
                        st += 1;
                    }
                    if st >= self.config.max_streams() {
                        log::debug!(
                            "Max clients reached, increase server->maxstreams ({})",
                            self.config.max_streams()
                        );
                        drop(conn); // explicit close
                        return Ok(None);
                    }
                    if self.maxstream < (st + 1) {
                        self.maxstream = st + 1;
                    }

                    let token = mio::Token(st);
                    let stream = &mut self.stream[st];
                    conn.set_nodelay(true)?;
                    stream.reinit(conn, addr, token);
                    self.streams += 1;

                    self.poll
                        .registry()
                        .register(stream, token, Interest::READABLE)?;
                }
                Err(err) if err.kind() == ErrorKind::WouldBlock => {}
                Err(err) => {
                    log::debug!("accept failed: {}", err);
                    return Err(err.into());
                }
            }
        }

        Ok(None)
    }

    pub fn get_msgtype(&self, value: i32) -> Option<&MsgType> {
        self.msgtypes.iter().find(|mt| mt.value == value)
    }

    pub fn queue_message_by_value(
        &mut self,
        value: i32,
        message: &[u8],
        msg: &Message,
    ) -> Result<(), Box<dyn Error>> {
        let Some(msgtype) = self.msgtypes.iter().find(|m| m.value == value) else {
            return Err(format!("could not find message type by value [{}]", value).into());
        };
        let stream = &mut self.stream[msg.token.0];
        stream.queue_message(msgtype, message)
    }

    pub fn send_messages_for(&mut self, msg: &Message) -> io::Result<usize> {
        let stream = &mut self.stream[msg.token.0];
        stream.send_queued_message(&self.config.inner)
    }

    pub fn send_messages(&mut self) -> io::Result<usize> {
        for i in (0..self.maxstream).rev() {
            if (self.stream[i].is_state_busy() || self.stream[i].is_state_end())
                && self.stream[i].to_send() > 0
            {
                return self.stream[i].send_queued_message(&self.config.inner);
            }
        }
        Ok(0)
    }
}
