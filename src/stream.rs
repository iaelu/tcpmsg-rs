use std::{
    cmp::Ordering,
    error,
    io::{self, ErrorKind, Read, Write},
    net::SocketAddr,
    ops::Sub,
    result::Result,
    str::from_utf8,
    time::Instant,
};

use mio::{event::Source, net::TcpStream};

use crate::{
    config::StreamConfig,
    message::Message,
    msgtype::{MsgType, DEFAULT_MAX_SIZE_SIZE},
};

#[derive(Debug, Clone, Copy)]
pub(crate) enum StreamState {
    Avail,
    Init,
    Busy,
    End,
    Close,
}

// Stream is not thread safe between receives and get_message,
//  or queue_message and send_queued_message. One must ensure
//  methods are used distinctly with some locks.
#[derive(Debug)]
pub(crate) struct Stream {
    inner: Option<TcpStream>,
    addr: Option<SocketAddr>,
    token: Option<mio::Token>,
    rcv_buf: Vec<u8>,
    rcv: usize,
    snd_buf: Vec<u8>,
    snd: usize,
    state: StreamState,
    rcv_isincomplete: bool,
    first_rcv: Instant,
    last_rcv: Instant,
}

impl Default for Stream {
    fn default() -> Self {
        Self {
            inner: None,
            addr: None,
            token: None,
            rcv_buf: Vec::new(),
            rcv: 0,
            snd_buf: Vec::new(),
            snd: 0,
            state: StreamState::Avail,
            rcv_isincomplete: false,
            first_rcv: Instant::now(),
            last_rcv: Instant::now(),
        }
    }
}

impl Stream {
    pub(crate) fn new(
        inner: Option<TcpStream>,
        addr: Option<SocketAddr>,
        token: Option<mio::Token>,
    ) -> Self {
        Stream {
            inner,
            addr,
            token,
            ..Stream::default()
        }
    }

    pub(crate) fn queue_message(
        &mut self,
        sig: &MsgType,
        message: &[u8],
    ) -> Result<(), Box<dyn error::Error>> {
        let mlen = message.len();
        let msglen = mlen.to_string();
        if msglen.len() > sig.sizesz {
            return Err("message data length > message signature length".into());
        }
        let mut size = [b'0'; DEFAULT_MAX_SIZE_SIZE];
        {
            let (_, right) = size.split_at_mut(sig.sizesz - msglen.len());
            right[..msglen.len()].copy_from_slice(msglen.as_bytes());
        }

        let left_alloc = self.snd_buf.len() - self.snd;
        if (sig.headersz + sig.sizesz + mlen) > left_alloc {
            let newlen = self.snd_buf.len() + ((sig.headersz + sig.sizesz + mlen) - left_alloc);
            self.snd_buf.resize_with(newlen, u8::default);
        }

        let mut written = 0;
        // message type/signature
        self.snd_buf[(self.snd + written)..(self.snd + written + sig.headersz)]
            .copy_from_slice(sig.sign);
        written += sig.headersz;
        // message size
        self.snd_buf[(self.snd + written)..(self.snd + written + sig.sizesz)]
            .copy_from_slice(&size[..sig.sizesz]);
        written += sig.sizesz;
        // message itself
        self.snd_buf[(self.snd + written)..(self.snd + written + mlen)].copy_from_slice(message);
        written += mlen;
        self.snd += written;

        Ok(())
    }

    pub(crate) fn send_queued_message(&mut self, config: &dyn StreamConfig) -> io::Result<usize> {
        let Some(stream) = self.inner.as_mut() else {
            return Ok(0);
        };

        if self.snd == 0 {
            if let StreamState::End = self.state {
                self.state = StreamState::Close;
                self.inner.take();
            }
            return Ok(0);
        }
        match self.state {
            StreamState::Busy | StreamState::End => {}
            _ => {
                return Err(io::Error::new(
                    ErrorKind::Other,
                    "trying to send on stream not ready",
                ))
            }
        }

        let to_send = self.snd.min(config.write_size());
        match stream.write(&self.snd_buf[..to_send]) {
            Ok(n) => {
                if n > 0 {
                    self.snd_buf.copy_within(n..self.snd, 0);
                    self.snd -= n;
                    if self.snd_buf.len() > (self.snd * 2) {
                        self.snd_buf.resize_with(self.snd * 2, u8::default)
                    }
                    if let StreamState::End = self.state {
                        self.state = StreamState::Close;
                        self.inner.take();
                    }
                }
                Ok(n)
            }
            Err(err) if is_intr_or_wouldblock(&err) => Ok(0), // try again
            Err(err) => {
                log::debug!("send_queued_messages, write error: {}", err);
                Err(err)
            }
        }
    }

    pub(crate) fn get_message(
        &mut self,
        msgtypes: &[MsgType],
        config: &dyn StreamConfig,
    ) -> Result<(Option<Message>, bool), Box<dyn error::Error>> {
        let state = self.state;
        match state {
            StreamState::Avail => return Ok((None, false)),
            StreamState::Init => {
                log::debug!("stream_getmsg() warning : we're trying to read a stream buffer marked as TCP_STREAM_INIT, we'll attempt to close it !");
                self.state = StreamState::Close;
                self.inner.take();
            }
            _ => {}
        }

        let timer = Instant::now();
        let mut closed = false;
        if matches!(self.state, StreamState::Close) && self.rcv == 0 {
            log::debug!(
                "OK, closing stream since buffer is empty and socket is marked as TCP_STREAM_CLOSE"
            );
            closed = true;
        } else if matches!(self.state, StreamState::Close) && self.rcv_isincomplete {
            log::debug!("OK, closing stream since buffer is incomplete and socket is marked as TCP_STREAM_CLOSE");
            closed = true;
        } else if self.rcv > config.max_msg_size() {
            log::debug!(
                "OK, closing stream since stream buffer size is > TCP_MAXMESSAGESZ ({})",
                config.max_msg_size()
            );
            closed = true;
        } else if let Some(timeout_inactive) = config.timeout_inactive() {
            if timer.sub(self.last_rcv) > timeout_inactive {
                log::debug!(
                    "OK, closing stream since stream has been inactive for more than {} us",
                    timeout_inactive.as_micros()
                );
                closed = true;
            }
        } else if let Some(timeout_active) = config.timeout_active() {
            if timer.sub(self.first_rcv) > timeout_active {
                log::debug!(
                    "OK, closing stream since stream has been active for more than {} us",
                    timeout_active.as_micros()
                );
                closed = true;
            }
        }
        if closed {
            self.inner.take();
            self.rcv_buf.clear();
            self.rcv_buf.shrink_to_fit();
            self.rcv = 0;
            self.state = StreamState::Avail;
            self.rcv_isincomplete = false;
            return Ok((None, closed));
        }

        self.last_rcv = timer;

        let mut stands = false;
        let mut mtcount = 0;
        for mt in msgtypes {
            mtcount += 1;
            if self.rcv < (mt.headersz + mt.sizesz) {
                stands = true;
                continue;
            }
            if mt.sign.cmp(&self.rcv_buf[..(mt.headersz)]) != Ordering::Equal {
                stands = true;
                continue;
            }

            let msgsz = from_utf8(&self.rcv_buf[mt.headersz..(mt.headersz + mt.sizesz)])?
                .parse::<usize>()?;
            if self.rcv < (mt.headersz + mt.sizesz + msgsz) {
                stands = true;
                continue;
            }

            let msg = Message {
                token: self.token.unwrap(),
                msgtype: mt.value,
                data: (self.rcv_buf[(mt.headersz + mt.sizesz)..(mt.headersz + mt.sizesz + msgsz)])
                    .to_owned(),
            };

            let oldmsgsz = mt.headersz + mt.sizesz + msgsz;
            self.rcv_buf.copy_within(oldmsgsz..self.rcv, 0);
            self.rcv -= oldmsgsz;
            if self.rcv_buf.len() > (self.rcv * 2) {
                self.rcv_buf.resize_with(self.rcv * 2, u8::default);
            }

            return Ok((Some(msg), false));
        }

        if mtcount >= msgtypes.len() {
            if stands {
                self.rcv_isincomplete = true;
            } else {
                self.state = StreamState::Close;
                self.inner.take();
                log::debug!(
                    "Stream sent a malformed message: [{:?}], closing stream",
                    self.rcv_buf
                );
                self.rcv_buf.clear();
                self.rcv_buf.shrink_to_fit();
                self.rcv = 0;
            }
        }
        Ok((None, false))
    }

    pub(crate) fn receive(&mut self, config: &dyn StreamConfig) -> io::Result<usize> {
        let Some(stream) = self.inner.as_mut() else {
            return Ok(0);
        };

        match self.state {
            StreamState::Busy | StreamState::End => {}
            _ => log::debug!("trying to receive on stream marked as {:?}", self.state),
        }

        let len = self.rcv_buf.len();
        let left_alloc = len - self.rcv;
        if config.read_size() > left_alloc {
            self.rcv_buf
                .resize_with(len + (config.read_size() - left_alloc), u8::default);
        }

        let res = stream.read(&mut self.rcv_buf[self.rcv..]);
        self.last_rcv = Instant::now();
        match res {
            Ok(0) => {
                log::debug!("remote has closed the connection: token={:?}", self.token);
                self.state = StreamState::Close;
                self.inner.take();
                Ok(0)
            }
            Ok(n) => {
                if !matches!(self.state, StreamState::End) {
                    self.state = StreamState::Busy;
                }
                self.rcv += n;
                Ok(n)
            }
            Err(err) if is_intr_or_wouldblock(&err) => Ok(0), // try again
            Err(err) => {
                log::debug!(
                    "error [{}] while receiving from remote: disconnecting",
                    &err
                );
                self.state = StreamState::Close;
                self.inner.take();
                Ok(0)
            }
        }
    }

    pub(crate) fn reinit(&mut self, conn: TcpStream, addr: SocketAddr, token: mio::Token) {
        self.state = StreamState::Init;
        self.token.replace(token);
        self.addr.replace(addr);
        self.inner.replace(conn);
        self.rcv_buf.clear();
        self.rcv_buf.shrink_to_fit();
        self.rcv = 0;
        self.snd_buf.clear();
        self.snd_buf.shrink_to_fit();
        self.snd = 0;
        self.rcv_isincomplete = false;
        self.last_rcv = Instant::now();
        self.first_rcv = Instant::now();
        self.state = StreamState::Busy;
    }

    pub(crate) fn set_busy(&mut self) {
        self.state = StreamState::Busy;
        self.last_rcv = Instant::now();
    }

    pub(crate) fn rcv(&self) -> usize {
        self.rcv
    }

    #[allow(dead_code)]
    pub(crate) fn addr(&self) -> Option<SocketAddr> {
        self.addr
    }

    #[allow(dead_code)]
    pub(crate) fn close(&mut self) -> io::Result<()> {
        self.state = StreamState::End;
        self.inner.take();
        Ok(())
    }

    pub(crate) fn to_send(&self) -> usize {
        self.snd
    }

    pub(crate) fn is_state_avail(&self) -> bool {
        matches!(self.state, StreamState::Avail)
    }

    #[allow(dead_code)]
    pub(crate) fn is_state_init(&self) -> bool {
        matches!(self.state, StreamState::Init)
    }

    pub(crate) fn is_state_busy(&self) -> bool {
        matches!(self.state, StreamState::Busy)
    }

    pub(crate) fn is_state_end(&self) -> bool {
        matches!(self.state, StreamState::End)
    }

    #[allow(dead_code)]
    pub(crate) fn is_state_close(&self) -> bool {
        matches!(self.state, StreamState::Close)
    }
}

fn is_intr_or_wouldblock(err: &io::Error) -> bool {
    matches!(err.kind(), ErrorKind::WouldBlock | ErrorKind::Interrupted)
}

impl Source for Stream {
    fn register(
        &mut self,
        registry: &mio::Registry,
        token: mio::Token,
        interests: mio::Interest,
    ) -> std::io::Result<()> {
        match self.inner.as_mut() {
            None => Ok(()),
            Some(stream) => stream.register(registry, token, interests),
        }
    }

    fn reregister(
        &mut self,
        registry: &mio::Registry,
        token: mio::Token,
        interests: mio::Interest,
    ) -> std::io::Result<()> {
        match self.inner.as_mut() {
            None => Ok(()),
            Some(stream) => stream.reregister(registry, token, interests),
        }
    }

    fn deregister(&mut self, registry: &mio::Registry) -> std::io::Result<()> {
        match self.inner.as_mut() {
            None => Ok(()),
            Some(stream) => stream.deregister(registry),
        }
    }
}
