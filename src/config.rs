use std::{
    fmt::Debug,
    ops::{Deref, DerefMut},
    time::Duration,
};

const DEFAULT_READ_SZ: usize = 65536;
const DEFAULT_WRITE_SZ: usize = 65536;
const DEFAULT_MAX_MESSAGE_SIZE: usize = 64 * 1024 * 1024;
const DEFAULT_EVENT_FREQ: Duration = Duration::from_micros(1);
const DEFAULT_ACCEPT_FREQ: Duration = Duration::from_micros(100);
const DEFAULT_TIMEOUT_INACTIVE: Option<Duration> = Some(Duration::from_secs(30));
const DEFAULT_TIMEOUT_ACTIVE: Option<Duration> = None;

#[derive(Debug, Clone, Copy)]
pub struct Config {
    read_sz: usize,
    write_sz: usize,
    max_msg_sz: usize,
    event_freq: Duration,
    timeout_inact: Option<Duration>,
    timeout_activ: Option<Duration>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            read_sz: DEFAULT_READ_SZ,
            write_sz: DEFAULT_WRITE_SZ,
            max_msg_sz: DEFAULT_MAX_MESSAGE_SIZE,
            event_freq: DEFAULT_EVENT_FREQ,
            timeout_inact: DEFAULT_TIMEOUT_INACTIVE,
            timeout_activ: DEFAULT_TIMEOUT_ACTIVE,
        }
    }
}

pub trait StreamConfig {
    fn read_size(&self) -> usize;
    fn set_read_size(&mut self, sz: usize) -> usize;

    fn write_size(&self) -> usize;
    fn set_write_size(&mut self, sz: usize) -> usize;

    fn max_msg_size(&self) -> usize;
    fn set_max_msg_size(&mut self, sz: usize) -> usize;

    fn event_frequence(&self) -> Duration;
    fn set_event_frequence(&mut self, freq: Duration) -> Duration;

    fn timeout_active(&self) -> Option<Duration>;
    fn set_timeout_active(&mut self, freq: Option<Duration>) -> Option<Duration>;

    fn timeout_inactive(&self) -> Option<Duration>;
    fn set_timeout_inactive(&mut self, freq: Option<Duration>) -> Option<Duration>;
}

impl Debug for dyn StreamConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("StreamConfig")
            .field("read_size", &self.read_size())
            .field("write_size", &self.write_size())
            .field("max_msg_size", &self.max_msg_size())
            .field("event_frequence (us)", &self.event_frequence().as_micros())
            .field("timeout_inactive", &self.timeout_inactive())
            .field("timeout_active", &self.timeout_active())
            .finish()
    }
}

impl StreamConfig for Config {
    fn read_size(&self) -> usize {
        self.read_sz
    }
    fn set_read_size(&mut self, sz: usize) -> usize {
        self.read_sz = sz;
        self.read_sz
    }

    fn write_size(&self) -> usize {
        self.write_sz
    }
    fn set_write_size(&mut self, sz: usize) -> usize {
        self.write_sz = sz;
        self.write_sz
    }

    fn max_msg_size(&self) -> usize {
        self.max_msg_sz
    }
    fn set_max_msg_size(&mut self, sz: usize) -> usize {
        self.max_msg_sz = sz;
        self.max_msg_sz
    }

    fn event_frequence(&self) -> Duration {
        self.event_freq
    }
    fn set_event_frequence(&mut self, freq: Duration) -> Duration {
        self.event_freq = freq;
        self.event_freq
    }

    fn timeout_active(&self) -> Option<Duration> {
        self.timeout_activ
    }
    fn set_timeout_active(&mut self, freq: Option<Duration>) -> Option<Duration> {
        self.timeout_activ = freq;
        self.timeout_activ
    }

    fn timeout_inactive(&self) -> Option<Duration> {
        self.timeout_inact
    }
    fn set_timeout_inactive(&mut self, freq: Option<Duration>) -> Option<Duration> {
        self.timeout_inact = freq;
        self.timeout_inact
    }
}

#[derive(Debug, Clone, Copy)]
pub struct SrvConfig {
    pub(crate) inner: Config,
    srv_port: u16,
    maxstreams: usize,
    accept_freq: Duration,
}

impl Default for SrvConfig {
    fn default() -> Self {
        Self {
            inner: Config::default(),
            srv_port: 0,
            maxstreams: 0,
            accept_freq: DEFAULT_ACCEPT_FREQ,
        }
    }
}

impl Deref for SrvConfig {
    type Target = Config;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl DerefMut for SrvConfig {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

pub trait ServerConfig: StreamConfig {
    fn server_port(&self) -> u16;
    fn set_server_port(&mut self, port: u16) -> u16;

    fn max_streams(&self) -> usize;
    fn set_max_streams(&mut self, max: usize) -> usize;

    fn accept_frequence(&self) -> Duration;
    fn set_accept_frequence(&mut self, freq: Duration) -> Duration;
}

impl Debug for dyn ServerConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ServerConfig")
            .field("server_port", &self.server_port())
            .field("max_streams", &self.max_streams())
            .field(
                "accept_frequence (us)",
                &self.accept_frequence().as_micros(),
            )
            .field("read_size", &self.read_size())
            .field("write_size", &self.write_size())
            .field("max_msg_size", &self.max_msg_size())
            .field("event_frequence (us)", &self.event_frequence().as_micros())
            .field("timeout_inactive", &self.timeout_inactive())
            .field("timeout_active", &self.timeout_active())
            .finish()
    }
}

impl ServerConfig for SrvConfig {
    fn server_port(&self) -> u16 {
        self.srv_port
    }
    fn set_server_port(&mut self, port: u16) -> u16 {
        self.srv_port = port;
        self.srv_port
    }

    fn max_streams(&self) -> usize {
        self.maxstreams
    }
    fn set_max_streams(&mut self, max: usize) -> usize {
        self.maxstreams = max;
        self.maxstreams
    }

    fn accept_frequence(&self) -> Duration {
        self.accept_freq
    }
    fn set_accept_frequence(&mut self, freq: Duration) -> Duration {
        self.accept_freq = freq;
        self.accept_freq
    }
}

impl StreamConfig for SrvConfig {
    fn read_size(&self) -> usize {
        self.inner.read_sz
    }
    fn set_read_size(&mut self, sz: usize) -> usize {
        self.inner.read_sz = sz;
        self.inner.read_sz
    }

    fn write_size(&self) -> usize {
        self.inner.write_sz
    }
    fn set_write_size(&mut self, sz: usize) -> usize {
        self.inner.write_sz = sz;
        self.inner.write_sz
    }

    fn max_msg_size(&self) -> usize {
        self.inner.max_msg_sz
    }
    fn set_max_msg_size(&mut self, sz: usize) -> usize {
        self.inner.max_msg_sz = sz;
        self.inner.max_msg_sz
    }

    fn event_frequence(&self) -> Duration {
        self.inner.event_freq
    }
    fn set_event_frequence(&mut self, freq: Duration) -> Duration {
        self.inner.event_freq = freq;
        self.inner.event_freq
    }

    fn timeout_active(&self) -> Option<Duration> {
        self.inner.timeout_activ
    }
    fn set_timeout_active(&mut self, freq: Option<Duration>) -> Option<Duration> {
        self.inner.timeout_activ = freq;
        self.inner.timeout_activ
    }

    fn timeout_inactive(&self) -> Option<Duration> {
        self.inner.timeout_inact
    }
    fn set_timeout_inactive(&mut self, freq: Option<Duration>) -> Option<Duration> {
        self.inner.timeout_inact = freq;
        self.inner.timeout_inact
    }
}
