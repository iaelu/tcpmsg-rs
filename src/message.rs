#[derive(Debug, Clone)]
pub struct Message {
    pub(crate) data: Vec<u8>,
    pub(crate) msgtype: i32,
    pub(crate) token: mio::Token,
}

impl Message {
    pub fn data(&self) -> &[u8] {
        &self.data[..]
    }
    pub fn msgtype(&self) -> i32 {
        self.msgtype
    }
}
