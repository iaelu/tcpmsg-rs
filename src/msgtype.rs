pub(crate) const DEFAULT_MAX_SIGN_SIZE: usize = 40;
pub(crate) const DEFAULT_MAX_SIZE_SIZE: usize = 10;

#[derive(Debug, Default, Clone, Copy)]
pub struct MsgType<'a> {
    pub(crate) sign: &'a [u8],
    pub(crate) headersz: usize,
    pub(crate) sizesz: usize,
    pub(crate) value: i32,
}

impl<'a> MsgType<'a> {
    pub(crate) fn new(value: i32, sign: &'a [u8], size: usize) -> Self {
        Self {
            value,
            headersz: sign.len(),
            sign,
            sizesz: size,
        }
    }
    pub fn sign(&self) -> &'a [u8] {
        self.sign
    }
    pub fn size(&self) -> usize {
        self.sizesz
    }
    pub fn value(&self) -> i32 {
        self.value
    }
}

pub struct RequestType<'a> {
    pub value: i32,
    pub sign: &'a [u8],
    pub size: usize,
}
